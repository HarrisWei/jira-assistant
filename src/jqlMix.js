export default {
  methods: {
    getJqlSuggestion (field, value, callback) {
      this.$http.get('/rest/api/2/jql/autocompletedata/suggestions', {
        fieldName: field,
        fieldValue: value
      }).then((response) => {
        if (typeof callback === 'function') callback(response.data)
      })
    }
  }
}
