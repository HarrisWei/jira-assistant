import Vue from 'vue'
import Router from 'vue-router'
import DailyOutput from '@/components/DailyOutput'
import SprintReport from '@/components/SprintReport'
import VueAxiosPlugin from 'vue-axios-plugin'

Vue.use(Router)

Vue.use(VueAxiosPlugin, {
  // baseURL: 'https://abcsproductdev.atlassian.net',
  auth: {
    username: 'weiyh@novasoftware.cn',
    password: 'YdjnAZwVHq79ziSdVR4ZD896'
  },
  headers: {
    'X-Atlassian-Token': 'no-check'
  },
  // request interceptor handler
  reqHandleFunc: config => config,
  reqErrorFunc: error => Promise.reject(error),
  // response interceptor handler
  resHandleFunc: response => {
    var temp = JSON.stringify(response.data)
    temp = temp.replace(/"tians@novasoftware.cn"/g, '"Ben"')
      .replace(/"zheng"/g, '"Zheng"')
      .replace(/"harris"/g, '"Harris"')
    response.data = JSON.parse(temp)
    return response
  },
  resErrorFunc: error => Promise.reject(error)
})

// var APIToken = 'weiyh@novasoftware.cn:YdjnAZwVHq79ziSdVR4ZD896' // need base64
export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/output-capacity'
    },
    {
      path: '/output-capacity',
      name: 'DailyOutput',
      component: DailyOutput
    },
    {
      path: '/sprint-report',
      name: 'SprintReport',
      component: SprintReport
    }
  ]
})
